import { getItemIdFromURL, convertToStringValue } from './Services';

describe('Services', () => {
  test('getItemIdFromURL should return last number after slash from url', () => {
    const result = getItemIdFromURL(
      'https://www.anapioficeandfire.com/api/characters/345'
    );
    expect(result).toBe('345');
  });

  describe('convertToStringValue', () => {
    test('It should return "-" if value is undefined', () => {
      const result = convertToStringValue();
      expect(result).toBe('-');
    });

    test('It should return string with values if value is an array', () => {
      const result = convertToStringValue(['a', 'b', 'c']);
      expect(result).toBe('a, b, c');
    });

    test('It should return string if value is number', () => {
      const result = convertToStringValue(11);
      expect(result).toBe('11');
    });

    test('It should return string if value is string', () => {
      const result = convertToStringValue('a');
      expect(result).toBe('a');
    });
  });
});
