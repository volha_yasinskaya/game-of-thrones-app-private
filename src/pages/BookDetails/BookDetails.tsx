import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { observer } from 'mobx-react';

import './BookDetails.scss';
import { BooksStore } from '../../store/BooksStore';
import BookModal from '../../components/BookModal/BookModal';
import { useCallback } from 'react';

const BookDetails: React.FC<{ store: BooksStore }> = observer(({ store }) => {
  const { bookId } = useParams<{ bookId: string }>();
  const {
    selectBook,
    increaseCharactersPageNumber,
    loadBookCharacters,
    selectedBook,
    selectedBookCharacters,
    areCharactersLoading,
    hasMoreCharactersToLoad,
    charactersPaginationNumber,
  } = store;
  const history = useHistory();

  useEffect(() => {
    selectBook(bookId);
  }, []);

  useEffect(() => {
    if (selectedBook) {
      loadBookCharacters();
    }
  }, [selectedBook, charactersPaginationNumber]);

  const goBack = useCallback(
    (e: any): void => {
      e.stopPropagation();
      history.goBack();
    },
    [history]
  );

  return (
    <>
      {store && store.selectedBook && (
        <BookModal
          selectedBook={selectedBook!}
          characters={selectedBookCharacters}
          closeModal={goBack}
          isLoading={areCharactersLoading}
          hasMore={hasMoreCharactersToLoad}
          increasePageNumber={increaseCharactersPageNumber}
        />
      )}
    </>
  );
});

export default BookDetails;
