import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import './Books.scss';
import BookCardList from '../../components/BookCardList/BookCardList';
import { BooksStore } from '../../store/BooksStore';

const Books: React.FC<{ store: BooksStore }> = observer(({ store }) => {
  const { books, loadBooks } = store;

  const [searchTerm, setSearchTerm] = useState('');
  const [filteredBooks, setFilteredBooks] = useState(books);

  useEffect(() => {
    loadBooks();
  }, []);

  useEffect(() => {
    const searchedBooks = books.filter((book) =>
      book.name.toLowerCase().includes(searchTerm)
    );
    setFilteredBooks(searchedBooks);
  }, [searchTerm, books]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const { t } = useTranslation();

  return (
    <div className="books">
      {store && store.books && !!store.books.length && (
        <>
          <div className="books__title">
            <h1>{t('Game of Thrones')}</h1>
            <input
              type="text"
              placeholder={t('Search Book')}
              value={searchTerm}
              onChange={handleChange}
            />
          </div>

          <BookCardList books={filteredBooks}></BookCardList>
        </>
      )}
    </div>
  );
});

export default Books;
