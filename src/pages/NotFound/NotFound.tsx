import React from 'react';

import './NotFound.scss';

export const NotFound: React.FC = () => (
  <div className="not-found">
    <h1>404</h1>
    <p>Oops! Something is wrong.</p>
  </div>
);
