import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import './Card.scss';

export interface CardItem {
  label: string;
  value: string;
}

export interface CardProps {
  title: string;
  items: CardItem[];
  toReadMore: { state: any; pathname: string };
  readMoreLabel?: string;
}

const Card: React.FC<CardProps> = React.memo(
  ({ title, items, toReadMore, readMoreLabel }) => {
    const { t } = useTranslation();

    const cardItems = items.map(({ label, value }, idx) => (
      <div key={idx} className="card__item">
        <span className="card__item-label">{label}:</span>
        <span className="card__item-value">{value}</span>
      </div>
    ));

    return (
      <div className="card">
        <h2>{title}</h2>
        <div className="card__content">{cardItems}</div>
        <Link to={toReadMore}>
          <button>{readMoreLabel ? readMoreLabel : t('More')}</button>
        </Link>
      </div>
    );
  }
);

export default Card;
