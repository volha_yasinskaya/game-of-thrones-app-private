import { shallow } from 'enzyme';
import BookCardList, { BookCardListProps } from './BookCardList';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (key: any) => key }),
}));

jest.mock('react-router-dom', () => ({
  useLocation: () => ({
    pathname: 'localhost:3000/example/path',
  }),
}));

const bookCardListProps: BookCardListProps = {
  books: [
    {
      name: 'TestName',
      released: '1996-08-01T00:00:00',
      numberOfPages: 798,
      authors: ['TestAuthor1', 'TestAuthor2'],
      url: 'https://www.anapioficeandfire.com/api/books/2',
    },
    {
      name: 'TestName2',
      released: '1996-08-01T00:00:00',
      numberOfPages: 745,
      authors: ['TestAuthor1', 'TestAuthor2'],
      url: 'https://www.anapioficeandfire.com/api/books/3',
    },
  ],
};

describe('<BookCardList />', () => {
  let component: any;

  beforeEach(() => {
    component = shallow(<BookCardList books={bookCardListProps.books} />);
  });

  test('It should mount', () => {
    expect(component.length).toBe(1);
  });

  test('It should render books array', () => {
    expect(component.find('.book-card-list').children().length).toBe(2);
  });
});
